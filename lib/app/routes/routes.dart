import 'package:flutter/widgets.dart';
import 'package:eco_boost/app/app.dart';
import 'package:eco_boost/home/home.dart';
import 'package:eco_boost/login/login.dart';

List<Page<dynamic>> onGenerateAppViewPages(
  AppStatus state,
  List<Page<dynamic>> pages,
) {
  switch (state) {
    case AppStatus.authenticated:
      return [HomePage.page()];
    case AppStatus.unauthenticated:
      return [LoginPage.page()];
  }
}
